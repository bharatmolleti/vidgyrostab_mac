#ifndef __PLATFORM_MEM_H_BASE__
#define __PLATFORM_MEM_H_BASE__

// allocate and log the memory block adress and size.
void* dmalloc__(int size);

// free and erase the block adress and size
void dfree__(void* mem);

// log all of the assignments,
// preferably call as the last function before returning to os.
void dprintmemlog_();


#endif // __PLATFORM_MEM_H_BASE__