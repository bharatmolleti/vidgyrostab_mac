#ifndef __PLATFORM_H_BASE__
#define __PLATFORM_H_BASE__

#define ENABLE_DEBUG

// bharatmolleti@gmail.com
// if any function or variable ends with __, it means use within the header only.
// if any function or variable ends with __, it means non-standard, unless you know why, don't use it.

#define PERROR	(1)
#define POK		(0)
#define PEOF	(2)

#include <stdio.h>

// Logging 
#define pprintf(...) printf( __VA_ARGS__);

#include <stdlib.h>
#include <string.h>

// Memory

#ifdef ENABLE_DEBUG

#include "platform_mem.h"

#define pmalloc(...) dmalloc__(__VA_ARGS__);
#define pfree(...)	dfree__(__VA_ARGS__);

#else 

#define pmalloc(...) malloc(__VA_ARGS__);
#define pfree(...)	free(__VA_ARGS__);

#endif

#define pmemcpy(...) memcpy(__VA_ARGS__);

// File
#define pfopen(...) fopen(__VA_ARGS__);
#define pfclose(...) fclose(__VA_ARGS__);

#endif // __PLATFORM_H_BASE__