// bharatmolleti@gmail.com
// if any function or variable ends with __, it means use within the header only.
// if any function or variable ends with __, it means non-standard, unless you know why, don't use it.

// Enable memory leakage checks
// compiler dependent, works on visual studio - 2008. c++98 compliant

#include "platform_mem.h"
#include <map>
#include <iostream>

std::map<void*, int> mem__; // stores the memory adresses and sizes.

// allocate and log the memory block adress and size.
void* dmalloc__(int size){
	void* mem = new unsigned char[size];
	if(mem){
		mem__.insert(std::make_pair(mem, size));
		return mem;
	}else{
		return 0;
	}
}

// free and erase the block adress and size
void dfree__(void* mem){
	if(mem){
		mem__.erase(mem);
		delete (unsigned char*)mem;
	}
}

// log all of the assignments,
// preferably call as the last function before returning to os.
void dprintmemlog_(){
	if(mem__.empty()){
		std::cout<< std::endl << "All Memory has been properly released" << std::endl;
	}else{
		for(std::map<void*, int>::iterator it= mem__.begin(); it!=mem__.end(); it++){
			std::cout << std::endl << "These blocks are leaking .. " << it->first << " of size ("<< it->second << ")" << std::endl ;
		}
	}
}
