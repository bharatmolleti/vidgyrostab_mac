#include "platform.h"
#include "platformtests.h"

void test_logging(){
	pprintf("\n hello world");
}

void test_memory(){
	void* mem = pmalloc(128);
	dprintmemlog_();
	
	pfree(mem);
	dprintmemlog_();
}