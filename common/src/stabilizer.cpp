#include "platform.h"
#include "meshwarp.h"
#include "stabilizer.h"

#include <math.h>

Matrix<double>* VidGyroStabilizer::convolve(Matrix<double>* signal, Matrix<double>* kernel)
{
	int result_len = signal->getElementCount()+ kernel->getElementCount() - 1;
	
	Matrix<double>* result = new Matrix<double>(signal->getColoumnCount()-1, result_len);
	double** arr_result = result->getBackingArray();

	double** arr_signal = signal->getBackingArray();
	double** arr_kernel = kernel->getBackingArray();
	
	int KernelLen = kernel->getElementCount();
	int SignalLen = signal->getElementCount();

	int n;

	for (n = 0; n < result_len; n++)
	{
		int kmin, kmax, k;

		arr_result[0][n] = 0;
		arr_result[1][n] = 0;
		arr_result[2][n] = 0;

		kmin = (n >= KernelLen - 1) ? n - (KernelLen - 1) : 0;
		kmax = (n < SignalLen - 1) ? n : SignalLen - 1;

		for (k = kmin; k <= kmax; k++)
		{
		  arr_result[0][n] += arr_signal[1][k] * arr_kernel[0][n - k];
		  arr_result[1][n] += arr_signal[2][k] * arr_kernel[0][n - k];
		  arr_result[2][n] += arr_signal[3][k] * arr_kernel[0][n - k];
		}
	}
	return result;
}


VidGyroStabilizer::VidGyroStabilizer(double fd, double gd, double fl){
	width_ 		= 	0;
	height_ 	= 	0;
	
	flength_ 	= 	fl;
	fdelay_		= 	fd;
	gdelay_		= 	gd;

	dth_		=	NULL;
	theta_		=	NULL;
	gyrotimetd_	=	NULL;

	warp_ 		=	NULL;
}

VidGyroStabilizer::~VidGyroStabilizer(){
	uninit();
}

int VidGyroStabilizer::calculate_sets(Matrix<double>* gyro, Matrix<double>* frame_time){	
	gyro_	= gyro;
	ftime_ 	= frame_time;
	
	double** arr_gyro 	= gyro_->getBackingArray();
	double** arr_ftime 	= ftime_->getBackingArray();
	
	Matrix<double>* g = (*gyro)(':', 1, 3);
	
	double sigma2 = 4000;
	
	Matrix<double> gauss(1, 241);
	double** arr_gauss = gauss.getBackingArray();

	double gauss_sum = 0;
	for(int i = -120, j = 0; i <= 120; i++, j++){
		double temp = (i*i)/sigma2;
		arr_gauss[0][j] = exp( -1.0 * temp);
		gauss_sum += arr_gauss[0][j];
	}
	for(int i = -120, j = 0; i <= 120; i++, j++){		
		arr_gauss[0][j] /= gauss_sum;
	}

	Matrix<double>* conv = convolve(gyro, &gauss);
	double** arr_conv = conv->getBackingArray();
	int start = (conv->getElementCount()/2) - (gyro->getElementCount()/2);
	int end = start + gyro->getElementCount()-1;
	double** arr_g = g->getBackingArray();
    for(int i = start, j = 0; i <= end; i++, j++){
		arr_g[0][j] -= arr_conv[0][i];
		arr_g[1][j] -= arr_conv[1][i];
		arr_g[2][j] -= arr_conv[2][i];
	}	
	
	Matrix<double> dgt(1, gyro->getElementCount()-1);
	double** arr_dgt = dgt.getBackingArray();
	for(int i = 0; i < dgt.getElementCount(); i++){
		arr_dgt[0][i] = arr_gyro[0][i+1] - arr_gyro[0][i] ;
    }

	Matrix<double> theta(3, dgt.getElementCount()+1);
	double** arr_theta = theta.getBackingArray();
	for(int i = 0; i < dgt.getElementCount(); i++){
		arr_theta[0][i+1] = ((arr_g[0][i] + arr_g[0][i+1])/2)*arr_dgt[0][i];
		arr_theta[1][i+1] = ((arr_g[1][i] + arr_g[1][i+1])/2)*arr_dgt[0][i];
		arr_theta[2][i+1] = ((arr_g[2][i] + arr_g[2][i+1])/2)*arr_dgt[0][i];
	}    

	for(int i = 0; i < theta.getElementCount(); i++){
		arr_theta[0][i] = (i==0)?0:(arr_theta[0][i]+arr_theta[0][i-1]);
		arr_theta[1][i] = (i==0)?0:(arr_theta[1][i]+arr_theta[1][i-1]);
		arr_theta[2][i] = (i==0)?0:(arr_theta[2][i]+arr_theta[2][i-1]);
	}

	Matrix<double> interp(theta.getColoumnCount(), frame_time->getElementCount());
	double** arr_interp = interp.getBackingArray();
	Matrix<double> gyrodiff(1, gyro->getElementCount());			
	double** arr_gyrodiff = gyrodiff.getBackingArray();
	for(int j = 0; j < gyro->getElementCount(); j++){
		arr_gyrodiff[0][j] = arr_gyro[0][j] - gdelay_;
	}
	for(int i = 0; i <interp.getColoumnCount(); i++){		
		double* yinterp = arr_interp[i];	
		double* xv = arr_gyrodiff[0];
		double* yv = arr_theta[i];
		double* x = arr_ftime[0];
		double number = 0;
		double* ydefault =&number;

		int m = gyro->getElementCount();
		int minterp = frame_time->getElementCount();
		MeshWarp::lininterp1f(yinterp,xv,yv,x,ydefault,m,minterp);
	}

	// final structs
	dth_ = new Matrix<double> (interp.getColoumnCount(), interp.getElementCount()-1);
	double** arr_dth = dth_->getBackingArray();
	for(int i = 0; i < dth_->getElementCount(); i++){
		arr_dth[0][i] = arr_interp[0][i+1] - arr_interp[0][i];
		arr_dth[1][i] = arr_interp[1][i+1] - arr_interp[1][i];
		arr_dth[2][i] = arr_interp[2][i+1] - arr_interp[2][i];
	}

	theta_ = new Matrix<double> (3, dgt.getElementCount()+1);
	double** arr_theta2 = theta_->getBackingArray();
	for(int i = 0; i < dgt.getElementCount(); i++){
		arr_theta2[0][i+1] = ((arr_gyro[1][i] + arr_gyro[1][i+1])/2)*arr_dgt[0][i];
		arr_theta2[1][i+1] = ((arr_gyro[2][i] + arr_gyro[2][i+1])/2)*arr_dgt[0][i];
		arr_theta2[2][i+1] = ((arr_gyro[3][i] + arr_gyro[3][i+1])/2)*arr_dgt[0][i];
    }
	
	for(int i = 0; i < theta_->getElementCount(); i++){
		arr_theta2[0][i] = (i==0)?0:(arr_theta2[0][i]+arr_theta2[0][i-1]);
		arr_theta2[1][i] = (i==0)?0:(arr_theta2[1][i]+arr_theta2[1][i-1]);
		arr_theta2[2][i] = (i==0)?0:(arr_theta2[2][i]+arr_theta2[2][i-1]);
	}
	
    gyrotimetd_ = new Matrix<double> (1, gyro->getElementCount());
    double** arr_gyrotimetd = gyrotimetd_->getBackingArray();
    for(int i = 0; i < gyrotimetd_->getElementCount(); i++){
        arr_gyrotimetd[0][i] =  arr_gyro[0][i] - gdelay_;
    }
	
	// finally
	delete conv;
	delete g;
	
	return POK;
}


int VidGyroStabilizer::init(int width, int height){
	width_ = width;
	height_ = height;

	warp_ = (void*) new MeshWarp(width_, height_);
	return POK;
}

int VidGyroStabilizer::uninit(){

	if(warp_){
		delete (MeshWarp*) warp_;
		warp_ = NULL;
	}

	if(dth_){		
		delete dth_;
		dth_ = NULL;
	}
	
	if(theta_){
		delete theta_;
		theta_ = NULL;
	}
	
	if(gyrotimetd_){
		delete gyrotimetd_;
		gyrotimetd_ = NULL;
	}

	return POK;
}

int VidGyroStabilizer::step(int f, RValues* values){
	if(f <0 || f > (ftime_->getElementCount()-2)){
		pprintf("ERROR! invalid frame number %d/%d", f, (ftime_->getElementCount()-2));
		return PERROR;
	};	
	
	if(dth_ == NULL || theta_ == NULL || gyrotimetd_ == NULL){
		pprintf("ERROR! calculate sets first");
		return PERROR;
	}

	if(warp_ == NULL){
		pprintf("ERROR! init warping module first.");
		return PERROR;
	}

	double** arr_gyro 	= gyro_->getBackingArray();
	double** arr_ftime 	= ftime_->getBackingArray();	
	double** arr_dth = dth_->getBackingArray();
	
	double* ptempgyro = arr_gyro[0];
	camtheta[0] = arr_dth[0][f];
	camtheta[1] = arr_dth[1][f];
	camtheta[2] = arr_dth[2][f];	
	double* ptemp_dtharr = camtheta;
		
	values->rowtheta = ((MeshWarp*)warp_)->meshwarp(
				ptemp_dtharr, 3,
				theta_,
				ptempgyro, gyro_->getElementCount(),
				gyrotimetd_,
				arr_ftime[0][f+1],
				gdelay_, fdelay_, flength_);    

	values->camtheta = ptemp_dtharr;

	return POK;
}
