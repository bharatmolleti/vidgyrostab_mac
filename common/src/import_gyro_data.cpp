#include "import_gyro_data.h"
#include "platform.h"

#include <stdio.h>

Matrix<double>* Gyro::import_gyro_data(const char* sFileName){
	Matrix<double>* gyro;

	FILE* fp = fopen(sFileName, "rb");
	if(!fp){
		pprintf("\n Failed to open the needed file : %s ", sFileName);
		return 0;
	}

	const int coloumnCount = 4;
	const int lineWidth = 128;
	const int WORD_LENGTH = 15;
	const int DELIMITERCOUNT = 4;

	char line[lineWidth];
	int nLineCount = 0;

	while ( fgets (line , lineWidth , fp) != NULL )
		nLineCount++;
	nLineCount -=1;
	fseek(fp, 0, SEEK_SET);

	gyro = new Matrix<double>(coloumnCount, (nLineCount+1));
	double** elements = gyro->getBackingArray();
	char tempWord[WORD_LENGTH];
	const char DELIMITER = ' ';

	int nCounter = 0;
	fseek(fp, 0, SEEK_SET);
	while(!feof(fp)){
		if(fgets (line , lineWidth , fp) == NULL) break;
		//printf("\n Read line : %s ", line);		
		int lineposition = 0;
		int delimiterCount = 0;
		int position = 0;
		while(delimiterCount < DELIMITERCOUNT){			
			tempWord[position] = line[lineposition];
			if(tempWord[position] == DELIMITER || tempWord[position] == '\n'){
				tempWord[position] = '\0';
				switch(delimiterCount){
					case 0:
						elements[0][nCounter] = atof(tempWord);
						break;
					case 1:
						elements[2][nCounter] = atof(tempWord);
						break;
					case 2:
						elements[1][nCounter] = atof(tempWord);
						break;
					case 3:						
						elements[3][nCounter] = atof(tempWord);						
						break;
					default:
						break;
				}
				delimiterCount++;
				if(delimiterCount>=DELIMITERCOUNT) break;
				position = 0;
			}else{
				position++;
			}
			lineposition ++;			
		}
		nCounter++;
		//if(nCounter >= nLineCount) break;
	}
	fclose(fp);

	return gyro;
}