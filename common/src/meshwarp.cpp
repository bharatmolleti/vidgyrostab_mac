#include "meshwarp.h"

#include <stdio.h>

MeshWarp::MeshWarp(int width, int height){
    //gl_ = new meshgles(width, height);
    rowtheta = new double[60];
}

MeshWarp::~MeshWarp(){
    delete rowtheta;
   //	delete gl_;
}

void MeshWarp::lininterp1f
    (double *yinterp, double *xv, double *yv, double *x, double *ydefault, int m, int minterp)
{
    int i, j;
    int nrowsinterp, nrowsdata;
    nrowsinterp = minterp;
    nrowsdata = m;
    for (i=0; i<nrowsinterp; i++){
        if((x[i] < xv[0]) || (x[i] > xv[nrowsdata-1])){
            yinterp[i] = *ydefault;
        }else{
            for(j=1; j<nrowsdata; j++){
                if(x[i]<=xv[j]){
                    yinterp[i] = (x[i]-xv[j-1]) / (xv[j]-xv[j-1]) * (yv[j]-yv[j-1]) + yv[j-1];
                    break;
                }
            }// for j
        }// else
    }// for i
}// lininterp1f


double* MeshWarp::meshwarp(double* dth, int dthCount,
                         Matrix<double>* theta,
                         double* gyro, int gyroCount,
                         Matrix<double>* gyrotimetd,
                         double frame_time,
                         double td, double ts, double fl){
    // % ft = linspace(frame_time + td - ts/2, frame_time + td + ts/2, 20);
   	static int f = 0;
    int elements_needed = 20;
    double start_time = frame_time + td - (ts/2);
    double end_time = frame_time + td + (ts/2);
    
    double spacing = (end_time - start_time)/(elements_needed-1);
    
    Matrix<double> ft(1, elements_needed);
    double** arr_ft = ft.getBackingArray();
    for(int i = 0; i < ft.getElementCount(); i++){
        arr_ft[0][i] =  (i==0)?start_time:(arr_ft[0][i-1] + spacing);
    }
    
    
    // % th = interp1(gyro_time, theta, ft, 'linear', 'extrap');
    Matrix<double> th (theta->getColoumnCount(), ft.getElementCount());
    double** arr_th = th.getBackingArray();
    double** arr_theta = theta->getBackingArray();
    for(int i = 0; i <th.getColoumnCount(); i++){
        double* yinterp = arr_th[i];
        double* xv = gyro;          // i/p element 1
        double* yv = arr_theta[i];	// i/p element 2
        double* x = arr_ft[0];      // i/p element 3
        double number = 0;
        double* ydefault =&number;
        
        int m = gyroCount;//xv_size
        int minterp = ft.getElementCount();// x size
        MeshWarp::lininterp1f(yinterp,xv,yv,x,ydefault,m,minterp);
    }
    
    double** arr_gyrotimetd = gyrotimetd->getBackingArray();
    // % thm = interp1(gyro_time - td, theta, frame_time, 'linear', 'extrap');
    Matrix<double> thm (theta->getColoumnCount(), 1);
    double** arr_thm = thm.getBackingArray();
    for(int i = 0; i <th.getColoumnCount(); i++){
        double* yinterp = arr_thm[i];
        double* xv = arr_gyrotimetd[0];	// i/p element 1
        double* yv = arr_theta[i];      // i/p element 2
        double* x = &frame_time;        // i/p element 3
        double number = 0;
        double* ydefault =&number;
        
        int m = gyrotimetd->getElementCount();//xv_size
        int minterp = 1;// x size
        MeshWarp::lininterp1f(yinterp,xv,yv,x,ydefault,m,minterp);
    }
    
    // % th = th - thm(ones(size(th, 1), 1), :);
    for(int i = 0; i <th.getElementCount(); i++){
        arr_th[0][i] -= arr_thm[0][0];
        arr_th[1][i] -=	arr_thm[1][0];
        arr_th[2][i] -= arr_thm[2][0];
    }
    
    //printf("\n\n\nTH : %3d -> ", f+1);
    for(int i = 0, k = 0; i < th.getElementCount(); i++){
        //printf("\n% 3d -> ", i+1);
        for(int j = 0; j < 3; j++){
            //printf("% 4.4lf, ", arr_th[j][i]);
            rowtheta[k] = arr_th[j][i];
            k++;
        }
        //printf("\n% 3d -> % 4.4lf, % 4.4lf, % 4.4lf ", i+1, arr_th[0][i], arr_th[1][i], arr_th[2][i]);
    }  
    
    camtheta = dth;
    
    //gl_->wrap( image, camtheta, rowtheta, fl);
    return rowtheta;
}