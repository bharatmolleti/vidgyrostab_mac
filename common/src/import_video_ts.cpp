#include "import_video_ts.h"

Matrix<double>* VideoTS::import_video_ts(const char* sFileName){
	Matrix<double>* frame_time = 0;

	FILE* fp = fopen(sFileName, "rb");
	if(!fp){
		pprintf("\n Failed to open the needed file : %s ", sFileName);
		return 0;
	}

	const int lineWidth = 128;
	const int coloumnCount = 2;

	char line[lineWidth];
	int nLineCount = 0;

	while ( fgets (line , lineWidth , fp) != NULL )
		nLineCount++;

	//pprintf("\n The file : %s contains : %d lines", sFileName, nLineCount);
	fseek(fp, 0, SEEK_SET);

	frame_time = new Matrix<double>(coloumnCount, (nLineCount));
	double** elements = frame_time->getBackingArray();

	int nCounter = 0;
	fseek(fp, 0, SEEK_SET);
	while(!feof(fp)){
		fgets (line , lineWidth , fp);
		//printf("\n Read line : %s ", line);
		elements[0][nCounter] = atof(line);
		nCounter++;
		if(nCounter>=nLineCount) break;
	}
	fclose(fp);

	return frame_time;
}