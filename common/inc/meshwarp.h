#ifndef __MESH_WARP_BASE__
#define __MESH_WARP_BASE__

#include "matrix.h"

class MeshWarp{
private:
    double* camtheta;
    double* rowtheta;
    
public:
    MeshWarp(int width, int height);
    ~MeshWarp();
    
    double* meshwarp(
				double* dth, int dthCount, 
				Matrix<double>* theta, 
				double* gyro, int gyroCount,
				Matrix<double>* gyrotimetd,
				double frametime,
				double td, double ts, double fl);

	static void lininterp1f(double *yinterp, double *xv, double *yv, double *x, double *ydefault, int m, int minterp);
};

#endif // __MESH_WARP_BASE__