#ifndef __VID_GYRO_STABILIZER__
#define __VID_GYRO_STABILIZER__

#include "matrix.h"
#include "gyrovalues.h"

class VidGyroStabilizer{
private:
	int width_;
	int height_;
	
	double 	fdelay_;	// frame delay
	double 	gdelay_;	// gyro delay
	double	flength_;	// focal length

	double camtheta[3];

	Matrix<double>* dth_;
	Matrix<double>* theta_;
	Matrix<double>* gyrotimetd_;

	Matrix<double>* gyro_;
	Matrix<double>* ftime_;

	void* warp_;
	
	Matrix<double>* convolve(Matrix<double>* signal, Matrix<double>* kernel);

public:
	VidGyroStabilizer(double fd, double gd, double f);
	~VidGyroStabilizer();
	int init(int width, int height);
	int uninit();
	int calculate_sets(Matrix<double>* gyro, Matrix<double>* frame_time);
	int step(int framenumber, RValues* values);
};

#endif	// __VID_GYRO_STABILIZER__