#ifndef __IMPORT_VIDEO_TS__
#define __IMPORT_VIDEO_TS__

#include "matrix.h"

class VideoTS{
public:
	static Matrix<double>* import_video_ts(const char* sFileName);
};

#endif // __IMPORT_VIDEO_TS__