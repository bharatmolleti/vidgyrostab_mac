#ifndef __MATRIX_H_BASE__
#define __MATRIX_H_BASE__

#include "platform.h"
#include <cstring>

template <class T>
class Matrix{
private:
	int result; // if 0, it means the last process on this ds was successful.
	T ** elements;
	int elementCount;	// number of elements in each of the coloumns.
	int coloumnCount;	// number of coloums this matrix has.
	Matrix();

public:
	Matrix(int nColumns, int nElements);
	~Matrix();
	Matrix* operator()(char c, int i, int j);
	T* operator[] (int i) const;
	T** getBackingArray();
	int getElementCount() const {return elementCount;};
	int getColoumnCount() const {return coloumnCount;};
	void print(const char* string = 0);
};

template <class T>
T** Matrix<T>::getBackingArray(){
	return elements;
}

template <class T>
Matrix<T>::Matrix(int nColumns, int nElements){	
	int totalSize = sizeof(T*) * nColumns;
	//printf("\n allocating %d coloumns for a total size of %d", nColumns, totalSize);
	elements = (T**)pmalloc(totalSize);	
	coloumnCount = nColumns;
	elementCount = nElements;
	// allocate space for each of the coloumns
	for(int i = 0; i < coloumnCount; i++){
		int columnSize = sizeof(T) * elementCount;
		//printf("\n allocating %d coloumn of size(%d) for a total size of %d", i, elementCount, columnSize);
		elements[i] = (T*)pmalloc(columnSize);
	}	
}

template <class T>
Matrix<T>::~Matrix(){
	if(elements){
		for(int i = 0; i < coloumnCount; i++){
			if(elements[i]){
				pfree((T*)elements[i]);
			}
		}
		pfree((T**)elements);
	}
}

template <class T>
	T* Matrix<T>::operator[] (int i) const{
	return elements[i];
}

// matlab style, matrix copy
// if the separator is ':' then this function creates a new array,
// containing all the rows of columns from 'begin' to 'end'.
// note that array subscripts are C/C++ style and not matlab style.
// simply stated, attempts to duplicate matlab style ; g = gyro(:, 1:3);
// use the function below as 
// Matrix * g = gyro(:, 0, 2);
template <class T>
Matrix<T>* Matrix<T>::operator()(char separator, int begin, int end) {
	Matrix* matrix = 0;
	if(separator == ':'){
		matrix = new Matrix<T>((end-begin)+1, elementCount);
		for(int i = begin, j= 0; i <= end; i++, j++){
			pmemcpy(matrix->elements[j], elements[i], sizeof(T) * elementCount);
		}
	}
	return matrix;
};

#endif // __MATRIX_H_BASE__