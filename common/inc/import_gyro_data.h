#ifndef __IMPORT_GYRO_DATA__
#define __IMPORT_GYRO_DATA__

#include "matrix.h"

class Gyro{
public:
	static Matrix<double>* import_gyro_data(const char* sFileName);
};

#endif // __IMPORT_GYRO_DATA__