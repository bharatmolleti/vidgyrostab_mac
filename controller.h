#ifndef __CONTROLLER_BASE__
#define __CONTROLLER_BASE__

#include "stabilizer.h"

// for a given device these parameters are fixed.
#define FRAME_DELAY (0.0207)
#define GYRO_DELAY (0.0105)
#define FOCAL_LENGTH (649.2773)

class Controller{
private:
	int width_;
	int height_;
	int fcount_;
	
	double gdelay_;
	double fdelay_;
	double flength_;

	Matrix<double>* gyro_;
	Matrix<double>* frametime_;
	VidGyroStabilizer* stabilizer_;
	
	static Controller controller;

	Controller();
	Controller(Controller&);
	Controller& operator = (const Controller&);
	
public:
	~Controller();
	
	static Controller& getInstance();

	// takes in a video file as input, parses out the gyro values, 
	// calculates the time stamps, passes this info to the GyroProcessor.
	// fills up the matrix of gyro and frametimes;
	int init(int width, int height, const char* sVideoFile);
	
	int step(RValues* value);
	int uninit();
};

#endif // __CONTROLLER_BASE__
