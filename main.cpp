//
//  main.cpp
//  GyroProcessor
//
//  Created by Bharat Kumar Molleti on 29/11/14.
//  Copyright (c) 2014 Bharat Kumar Molleti. All rights reserved.
//


//
//  main.cpp
//  stabtest
//
//  Created by Bharat Kumar Molleti on 29/11/14.
//  Copyright (c) 2014 Bharat Kumar Molleti. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <vector>

#include "controller.h"
#include "gyrovalues.h"

#include <GLUT/glut.h>
#include <boost/la/all.hpp>


int printedneeded = 0;

using namespace boost::la;
typedef vec<double,2> vec2;
typedef vec<double,3> vec3;
typedef mat<double,3,3> mat33;

static std::vector<vec3> mesh_verts;
static std::vector<vec2> mesh_uv;
static std::vector<GLushort> mesh_strip;

template <typename T>
vec<T,2> Vec(T x, T y) { vec<T,2> v = {x, y}; return v; };

template <typename T>
vec<T,3> Vec(T x, T y, T z) { vec<T,3> v = {x, y, z}; return v; };


void InitMesh(int width, int height, int num_rows, double focal_len)
{
    double w = width;
    double h = height;
    double dh = h / (num_rows-1);
    int num_cols = (int)round(width / dh) + 1;
    double dw = w / (num_cols-1);
    
    double du = 1.0 / (num_cols-1);
    double dv = 1.0 / (num_rows-1);
    
    mesh_verts.clear();
    mesh_uv.clear();
    mesh_verts.reserve(num_rows * num_cols);
    mesh_uv.reserve(num_rows * num_cols);
    
    double y = -h/2;
    double v = 0;
    for (int r=0; r < num_rows; ++r)
    {
        double x = -w/2;
        double u = 0;
        for (int c=0; c < num_cols; ++c)
        {
            mesh_verts.push_back(Vec(x, y, -focal_len));
            mesh_uv.push_back(Vec(u, v));
            
            x += dw;
            u += du;
        }
        y += dh;
        v += dv;
    }
    
    mesh_strip.clear();
    //mesh_verts.reserve(num_rows * num_cols * 2 + num_rows * 2);
    mesh_strip.reserve(num_rows * num_cols * 2 + num_rows * 2);
    for (int r=1; r < num_rows; ++r)
    {
        mesh_strip.push_back(num_cols*r);
        for (int c=0; c < num_cols; ++c)
        {
            mesh_strip.push_back(num_cols*r + c);
            mesh_strip.push_back(num_cols*(r-1) + c);
        }
        mesh_strip.push_back(num_cols*(r-1) + num_cols - 1);
    }
}

std::vector<vec3> WarpMesh(vec3* theta, int num_rows)
{
    std::vector<vec3> warped;
    warped.reserve(mesh_verts.size());
    int num_cols = mesh_verts.size() / num_rows;
    
    for (int r=0; r < num_rows; ++r)
    {
        mat33 R = rotz_matrix<3>(-*theta|Z) *
        roty_matrix<3>(*theta|X) *
        rotx_matrix<3>(*theta|Y);
        for (int c=0; c < num_cols; ++c)
        {
            warped.push_back(R * mesh_verts[num_cols*r + c]);
        }
        ++theta;
    }
    return warped;
}


typedef struct NeededData{
    int width;
    int height;
    
    double focallen;
    unsigned char* rawImage;
}GLData;

GLData* pgldata;


// texture loading - unloading block
GLuint static LoadTexture (int width, int height, unsigned char* image){
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
                 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    
    return texture;
}

void static FreeTexture( GLuint texture ){
    glDeleteTextures( 1, &texture );
}

int stop(){
    free(pgldata->rawImage);
    pgldata->rawImage = NULL;
    
    Controller& controller = Controller::getInstance();
    controller.uninit();
    
    delete pgldata;
    pgldata = NULL;
    
    mesh_verts.clear();
    mesh_uv.clear();
    mesh_strip.clear();
    
    return POK;
}

void display () {
    if(pgldata){
        glClearColor (0.0,0.0,0.0,1.0);
        glClear (GL_COLOR_BUFFER_BIT);
        glLoadIdentity();
        //gluLookAt (0.0, 0.0, 4.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
        
        RValues values;
        Controller& controller = Controller::getInstance();
        if(POK == controller.step( &values )){
            
            InitMesh(pgldata->width, pgldata->height, 20, pgldata->focallen);
            std::vector<vec3> warped = WarpMesh((vec3*)values.rowtheta, 20);
            
            GLuint texture = LoadTexture( pgldata->width, pgldata->height, pgldata->rawImage  );
            
            glEnable( GL_TEXTURE_2D );
            glDisable(GL_DEPTH_TEST);
            glBindTexture( GL_TEXTURE_2D, texture ); //bind the texture
            
            glPushMatrix();
           
            glRotatef(values.camtheta[0]*180/M_PI, 1, 0, 0);
            glRotatef(values.camtheta[1]*180/M_PI, 0, 1, 0);
            glRotatef(values.camtheta[2]*180/M_PI, 0, 0, -1);
#if 1
            glDisableClientState(GL_COLOR_ARRAY);
            glEnableClientState(GL_VERTEX_ARRAY);
            glVertexPointer(3, GL_DOUBLE, 0, &warped[0]);
            if(printedneeded%30 == 0){
                pprintf("\n The size of warped is %ld ", warped.size());
                FILE* fp = fopen("warped.txt", "ab");
                if(fp){
                    fprintf(fp, "\n\n %3d -> \n", printedneeded);
                    for(int i = 0; i < warped.size(); i++){
                        vec<double, 3> demo = warped[i];
                        double a = demo|X;
                        double b = demo|Y;
                        double c = demo|Z;
                        fprintf(fp, "% 3.4lf , % 3.4lf, % 3.4lf \n", a, b, c);
                    }
                    fclose(fp);
                    fp = NULL;
                }
            }
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);
            glTexCoordPointer(2, GL_DOUBLE, 0, &mesh_uv[0]);
            if(printedneeded%30 == 0){
                pprintf("\n the sizeof mesh_uv is %ld ", mesh_uv.size());
                FILE* fp = fopen("meshuv.txt", "ab");
                if(fp){
                    fprintf(fp, "\n\n %3d -> \n", printedneeded);
                    for(int i = 0; i < mesh_uv.size(); i++){
                        vec<double, 2> demo = mesh_uv[i];
                        double a = demo|X;
                        double b = demo|Y;
                        fprintf(fp, "% 3.4lf , % 3.4lf \n", a, b);
                    }
                    fclose(fp);
                    fp = NULL;
                }

            }
            glBindTexture(GL_TEXTURE_2D, texture);
            
            glDrawElements(GL_TRIANGLE_STRIP, mesh_strip.size(), GL_UNSIGNED_SHORT, &mesh_strip[0]);
            if(printedneeded%30 == 0){
                pprintf("\n The size of mesh_strip is %ld ", mesh_strip.size());
                FILE* fp = fopen("meshstrip.txt", "ab");
                if(fp){
                    fprintf(fp, "\n\n %3d -> \n", printedneeded);
                    for(int i = 0; i < mesh_strip.size(); i++){
                        fprintf(fp, "% 3d , ", mesh_strip[i]);
                        if(i%5 == 0) fprintf(fp, "\n");
                    }
                    fclose(fp);
                    fp = NULL;
                }
            }
            printedneeded++;
#else
            glBegin( GL_QUADS );
            glTexCoord2d(0.0,0.0); glVertex2d(-1.0,-1.0);
            glTexCoord2d(1.0,0.0); glVertex2d(+1.0,-1.0);
            glTexCoord2d(1.0,1.0); glVertex2d(+1.0,+1.0);
            glTexCoord2d(0.0,1.0); glVertex2d(-1.0,+1.0);
            glEnd();
#endif
            
            glPopMatrix();
            glutSwapBuffers();
            
            FreeTexture( texture );
        }else{
            stop();
            printf("Closed all allocated stuff, EOF Reached!");
        }
    }
}

void reshape (int w, int h) {
    glViewport (0, 0, (GLsizei)w, (GLsizei)h);
    
    glMatrixMode (GL_PROJECTION);
    
    if(pgldata){
        double f = pgldata->focallen;
        glFrustum(-w/(2*f), w/(2*f), -h/(2*f), h/(2*f), 1, f*4);
    }else{
        printf("\n GLData Struct is NULL");
    }
    
    glMatrixMode (GL_MODELVIEW);
}

#define WIDTH (640)
#define HEIGHT (480)

#define FOCAL_LEN (649.2773)



int start (int argc, char **argv) {
    
    pgldata = new GLData();
    
    pgldata->width = WIDTH;
    pgldata->height = HEIGHT;
    pgldata->focallen = FOCAL_LEN;
    
    Controller& controller = Controller::getInstance();
    controller.init( 640, 480, "/sdcard/demo.mp4");
    
    pgldata->rawImage = (unsigned char*) malloc(WIDTH * HEIGHT * 4);
    if(pgldata->rawImage== NULL){
        printf("\n Insufficient memory for Image");
        return -1;
    }
    
    FILE* fp = fopen("raw.raw", "rb");
    if(fp){
        fread(pgldata->rawImage, 1, (WIDTH*HEIGHT*3), fp);
        fclose(fp);
    }else{
        printf("\n Failed to open the needed texture file");
        return -1;
    }
    
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_SINGLE);
    glutInitWindowSize (WIDTH, HEIGHT);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("A basic OpenGL Window");
    glutDisplayFunc (display);
    glutIdleFunc (display);
    glutReshapeFunc (reshape);
    glutMainLoop ();
    return 0;
}

int main(int argc, const char * argv[]) {
    // insert code here...
    start(argc, (char**)argv);
    return 0;
}