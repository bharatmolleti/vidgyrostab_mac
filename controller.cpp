#include "controller.h"
#include "import_gyro_data.h"
#include "import_video_ts.h"

Controller Controller::controller;

Controller::Controller(){
	width_ 		= 	0;
	height_ 	= 	0;
	fcount_ 	= 	-1;
	
	gdelay_ 	= 	GYRO_DELAY;
	fdelay_		=	FRAME_DELAY;
	flength_ 	= 	FOCAL_LENGTH;

	gyro_ 		= 	NULL;
	frametime_ 	= 	NULL;
}

Controller& Controller::getInstance(){
		return controller;
}

Controller::~Controller(){
	uninit();
}

int Controller::init(int width, int height, const char* sVideoFile){
	width_ = width;
	height_ = height;
	
    const char* sGyroFile = "gyro.log";
    const char* sFrameStamps = "framestamps.txt";
	
	gyro_ = Gyro::import_gyro_data (sGyroFile);
	if(gyro_ == 0){
		pprintf("ERROR! Failed to open the needed gyroscope data file.");
		return PERROR;
	}	
	
	frametime_ = VideoTS::import_video_ts(sFrameStamps);
	if(frametime_ == 0){
		pprintf("ERROR! Failed to open the needed video timestamps data file.");
		return PERROR;
	}	

	stabilizer_ = new VidGyroStabilizer (fdelay_, gdelay_, flength_);
	stabilizer_->init (width_, height_);
	stabilizer_->calculate_sets(gyro_, frametime_);
	
	return POK;	
}

int Controller::uninit(){
	if(stabilizer_){
		stabilizer_->uninit();
		delete stabilizer_;
		stabilizer_ = NULL;
	}

	if(frametime_){
		delete frametime_;
		frametime_ = NULL;
	}
	
	if(gyro_){
		delete gyro_;
		gyro_ = NULL;
	}

	return POK;
}

int Controller::step(RValues* values){	
	if(stabilizer_ == NULL){
		pprintf("ERROR! Initialize Controller first.");
		return PERROR;
	}
	
	if(fcount_ >= (frametime_->getElementCount()-2)){
		pprintf("ERROR! Exceeded max frame count.(%d/%d)  EOS", fcount_, (frametime_->getElementCount()-2));
		return PEOF;
	}else{
		fcount_++;
	}

	int result = stabilizer_->step(fcount_, values);
	
	return result;
}
